/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test;

import java.time.LocalDate;
import java.time.Month;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Symon
 */
public class JobServiceTest {
    
    public JobServiceTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    /**
     * Test of checkEnableTime method, of class JobService.
     */
    @org.junit.jupiter.api.Test
    public void testCheckEnableTimeTodayIsBetweenStartTimeAndEndTime() {
        System.out.println("TodayIsBetweenStartTimeAndEndTime");
        // Arrange
        LocalDate StartTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 3);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(StartTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    
    @org.junit.jupiter.api.Test
    public void testCheckEnableTimeTodayIsBeforeStartTime() {
        System.out.println("TodayIsBeforeStartTime");
        // Arrange
        LocalDate StartTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 30);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(StartTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    
    @org.junit.jupiter.api.Test
    public void testCheckEnableTimeTodayIsAfterEndTime() {
        System.out.println("TodayIsAfterEndTime");
        // Arrange
        LocalDate StartTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 6);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(StartTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    
    @org.junit.jupiter.api.Test
    public void testCheckEnableTimeTodayIsEqualStartTime() {
        System.out.println("TodayIsEqualStartTime");
        // Arrange
        LocalDate StartTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 1, 31);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(StartTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    
    @org.junit.jupiter.api.Test
    public void testCheckEnableTimeTodayIsEqualEndTime() {
        System.out.println("TodayIsEqualEndTime");
        // Arrange
        LocalDate StartTime = LocalDate.of(2021, 1, 31);
        LocalDate endTime = LocalDate.of(2021, 2, 5);
        LocalDate today = LocalDate.of(2021, 2, 5);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(StartTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
}
